import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  locationTitles: { title: string, value: boolean }[] = [];
  category: number = 0;
  selectedCategory: string;
  subcategory: number = 0;
  selectedSubCategory: string;

  constructor() { }

  ngOnInit() {
    this.locationTitles.push({ title: '5th Avenue, New York, NY', value: false });
    this.locationTitles.push({ title: 'Tysons Corner Center, VA', value: false });
  }

  categoryClick(index, name) {
    if (this.category === index) {
      this.selectedCategory = '';
      this.category = 0;
    }
    else {
      this.category = index;
      this.selectedCategory = name;
    }
  }

  subCategoryClick(index, name) {
    if (this.subcategory === index && this.selectedSubCategory === name) {
      this.selectedSubCategory = '';
      this.locationTitles[0].value = false;
      this.subcategory = 0;
    }
    else {
      this.subcategory = index;
      this.selectedSubCategory = name;
      this.locationTitles[0].value = true;
    }
  }

  confirm() {
    alert(`Congratulations, you've successfully submitted ${this.selectedSubCategory} ${this.selectedCategory}  request`);
  }
}
